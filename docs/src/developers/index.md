# For developers

## Java library

Library's artifacts are published to maven central. The library can be downloaded with maven as dependency:

```xml
<dependency>
    <groupId>org.kravemir.svg.labels</groupId>
    <artifactId>lablie</artifactId>
    <version>0.4.0</version>
</dependency>
```

With gradle:

```groovy
compile group: 'org.kravemir.svg.labels', name: 'lablie', version: '0.4.0'
```

 See more details at [at search.maven.org][search-maven-org-by-group], or [at mvnrepository.com][mvnrepository-com-group].

## Javadoc

Javadocs can found at:

* [library](../javadoc/library) ([all classes](../javadoc/library/allclasses-noframe.html))
* [tool](../javadoc/tool) ([all classes](../javadoc/tool/allclasses-noframe.html))


[search-maven-org-by-group]: https://search.maven.org/search?q=org.kravemir.svg.labels
[mvnrepository-com-group]: https://mvnrepository.com/artifact/org.kravemir.svg.labels
