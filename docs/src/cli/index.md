# Command Line Tool

Command line tool `lablie` groups multiple sub-commands to work with labels:
                  
* [instance](command/instance.md) command instantiates SVG template with provided data,
* [tile](command/tile.md) SVG to meet specified print-paper layout,
* [project](command/project.md) support for complex usages.

## Reference 

Output of the `lablie --help`: 

```
{{#include help.txt}}
```
